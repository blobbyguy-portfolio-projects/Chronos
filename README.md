# Chronos #

Chronos is a management tool for configuring and designing a sequence of courses for efficiently completing a degree.

It uses Blazor along with a sophisticated constraint propagation algorithm in order to arrange a given set of courses into manageable states.

EDIT: demo no longer live

### Contributors ###

* Aiden Fuller
* Sam Gillard
* Josh Roworth
* Asim Faiaz
